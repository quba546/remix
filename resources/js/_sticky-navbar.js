// sticky navbar
$(window).scroll(function () {
  let $selector = $('#header');
  if ($(this).scrollTop() > $selector.height()) {
      //console.log($selector.height());
      $('#navbar_top').addClass("fixed-top");
      // add padding top to show content behind navbar
      $('body').css('padding-top', $('.navbar').outerHeight() + 'px');
  } else {
      $('#navbar_top').removeClass("fixed-top");
      // remove padding top from body
      $('body').css('padding-top', '0');
  }
});