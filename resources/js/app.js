require('./bootstrap');

require('./_aos');

require('./_footer');

require('./_sticky-navbar');

require('./_fb-widget');

require('./_toggler-sidebar-admin');

require('./_scroll-top-btn');

require('./_gallery');

require('./_admin-player-list-btn');

require('./_admin-matches-form');

require('./_csrf-token');
