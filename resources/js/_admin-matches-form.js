// logic for last and upcoming matches forms in admin panel
window.onload = function () {
  let matchType = document.getElementById('match-type');
  if (matchType) {
      matchType.onchange = function () {
          let element = document.getElementById("date-div-toggler");
          // The code of your function
          if (matchType.value === "1") {
              document.getElementById("league-matches").style.display = "block";
              element.className = element.className.replace(/\bcol-md-12\b/g, "col-md-6");
          } else {
              document.getElementById("league-matches").style.display = "none";
              element.className = element.className.replace(/\bcol-md-6\b/g, "col-md-12");
          }
      }
  }
}