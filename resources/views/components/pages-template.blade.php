<div class="container">
    <div class="row mx-auto">
        <div class="col-12 mt-5 mb-5 bg-white shadow-lg">
            <div class="text-center p-3">
                <h2 class="text-uppercase font-weight-bold mt-3">{{ $title }}</h2>
                <hr class="hr-text">
                {{ $slot }}
            </div>
        </div>
    </div>
</div>
